#!/bin/bash

# $1 = Source Folder
# $2 = Destination Folder
# $3 = File List

IFS=$'\n'
curFolder = ''
for f in $(cat $3)
do
#	trim_f=$(echo -n $f | tr -d "\n")
#	if [ $trim_f = "" ]
#	then
#		continue
#	fi
	f=${f//[$'\r\n']}
	if [[ $f == /* ]]
	then
		mkdir -p $2$f
		curFolder=$f
	else
		echo $1$curFolder/$f
		echo $2$curFolder
		cp "$1$curFolder/$f" "$2$curFolder/"
	fi
done	