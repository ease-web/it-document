Go to LoadBalancer/nfs node (192.168.225.113)
cd /nfs
mkdir eab-time-sheet-qa-db-pv001
chown -R nfsnobody eab-time-sheet-qa-db-pv001
chgrp -R nfsnobody eab-time-sheet-qa-db-pv001
chmod -R 2775 eab-time-sheet-qa-db-pv001
--------
mkdir eab-time-sheet-prod-db-pv001
chown -R nfsnobody eab-time-sheet-prod-db-pv001
chgrp -R nfsnobody eab-time-sheet-prod-db-pv001
chmod -R 2775 eab-time-sheet-prod-db-pv001



vi /etc/exports
----Modify exports:----
add a row:
/nfs/eab-time-sheet-qa-db-pv001 *(rw,sync,wdelay,hide,nocrossmnt,secure,root_squash,no_all_squash,no_subtree_check,secure_locks,acl,no_pnfs,anonuid=65534,anongid=65534,sec=sys,secure,root_squash,no_all_squash)
----
exportfs -ra
vi /var/lib/nfs/etab
----verify exports:----
/nfs/eab-time-sheet-qa-db-pv001 *(rw,sync,wdelay,hide,nocrossmnt,secure,root_squash,no_all_squash,no_subtree_check,secure_locks,acl,no_pnfs,anonuid=65534,anongid=65534,sec=sys,secure,root_squash,no_all_squash)
----
showmount -e
service nfs restart


Go to master node#1 (192.168.225.114)
cd /root
oc project eab-time-sheet
oc get pv
oc get pvc
create a yaml file:
---------------
apiVersion: v1
kind: PersistentVolume
metadata:
  name: eab-time-sheet-qa-db-pv001
spec:
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  nfs:
    path: /nfs/eab-time-sheet-qa-db-pv001
    server: 192.168.225.113
    readOnly: false
-------

oc create -f eab-time-sheet-qa-db-pv001.yaml
oc get pv
ensure a pv named eab-time-sheet-qa-db-pv001 has status 'available'
In Openshift Web console, add to project mongodb (persistent) and the pvc claim will be generated automatically (check storage)



