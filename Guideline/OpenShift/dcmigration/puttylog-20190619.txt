login as: root
root@192.168.225.113's password:
Last login: Wed Jun 19 17:49:12 2019 from lb.ocp.eabdc
[root@lb ~]# find / -type f -exec grep -l "is different from OCP image" {} \
> ;
find: missing argument to `-exec'
[root@lb ~]# find / -type f -exec grep -l "is different from OCP image" {}
find: missing argument to `-exec'
[root@lb ~]# grep -inr "is different from OCP image" /
grep: /proc/fs/nfsd/unlock_filesystem: Invalid argument
grep: /proc/fs/nfsd/unlock_ip: Invalid argument
grep: /proc/fs/nfsd/filehandle: Invalid argument
grep: /proc/sys/fs/binfmt_misc/register: Invalid argument
grep: /proc/sys/net/ipv4/route/flush: Permission denied
grep: /proc/sys/net/ipv6/conf/all/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/default/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/docker0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/eth0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/lo/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/route/flush: Permission denied
grep: /proc/sys/vm/compact_memory: Permission denied
^C
[root@lb ~]# grep -r "is different from OCP image" /
grep: /proc/fs/nfsd/unlock_filesystem: Invalid argument
grep: /proc/fs/nfsd/unlock_ip: Invalid argument
grep: /proc/fs/nfsd/filehandle: Invalid argument
grep: /proc/sys/fs/binfmt_misc/register: Invalid argument
grep: /proc/sys/net/ipv4/route/flush: Permission denied
grep: /proc/sys/net/ipv6/conf/all/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/default/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/docker0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/eth0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/lo/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/route/flush: Permission denied
grep: /proc/sys/vm/compact_memory: Permission denied
^C
[root@lb ~]# pwd
/root
[root@lb ~]# cd /
[root@lb /]# grep -inr "is different from OCP image" /
grep: /proc/fs/nfsd/unlock_filesystem: Invalid argument
grep: /proc/fs/nfsd/unlock_ip: Invalid argument
grep: /proc/fs/nfsd/filehandle: Invalid argument
grep: /proc/sys/fs/binfmt_misc/register: Invalid argument
grep: /proc/sys/net/ipv4/route/flush: Permission denied
grep: /proc/sys/net/ipv6/conf/all/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/default/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/docker0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/eth0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/lo/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/route/flush: Permission denied
grep: /proc/sys/vm/compact_memory: Permission denied
^C
[root@lb /]# find / -type f | xargs grep 'is different from OCP image'
grep: /proc/fs/nfsd/unlock_filesystem: Invalid argument
grep: /proc/fs/nfsd/unlock_ip: Invalid argument
grep: /proc/fs/nfsd/filehandle: Invalid argument
grep: /proc/sys/fs/binfmt_misc/register: Invalid argument
grep: /proc/sys/net/ipv4/route/flush: Permission denied
grep: /proc/sys/net/ipv6/conf/all/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/default/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/docker0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/eth0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/lo/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/route/flush: Permission denied
grep: /proc/sys/vm/compact_memory: Permission denied
^C
[root@lb /]# cd /usr/share
[root@lb share]# ls
aclocal                      groff                 omf
alsa                         grub                  os-prober
anaconda                     haproxy               p11-kit
ansible                      hwdata                perl5
applications                 i18n                  pixmaps
atomic-openshift-utils       icons                 pkgconfig
augeas                       idl                   pki
authconfig                   info                  plymouth
awk                          java                  polkit-1
backgrounds                  java-1.5.0            pygobject
bash-completion              java-1.6.0            python-dmidecode
containers                   java-1.7.0            redhat-logos
container-storage-setup      java-1.8.0            redhat-release
cracklib                     javadoc               rhel
dbus-1                       java-ext              rhn
desktop-directories          java-utils            rhsm-plugins
dict                         javazi                selinux
doc                          javazi-1.8            setools-3.3
emacs                        jvm                   setuptool
empty                        jvm-commmon           sos
file                         kde4                  sounds
firewalld                    kdump                 systemd
firstboot                    keyutils              systemtap
fish                         libdrm                tabset
games                        licenses              terminfo
gcc-4.8.2                    locale                themes
gcc-4.8.5                    lua                   tuned
gdb                          magic                 vim
GeoIP                        man                   wallpapers
gettext                      maven-effective-poms  X11
gettext-0.19.8               maven-fragments       xsessions
ghostscript                  maven-poms            yum-cli
git-core                     mime                  yum-plugins
glib-2.0                     mime-info             zoneinfo
gnome                        misc                  zsh
gnome-background-properties  mysql
gnupg                        oci-umount
[root@lb share]# cd ansible
[root@lb ansible]# ls
openshift-ansible  plugins  roles
[root@lb ansible]# cd open*
[root@lb openshift-ansible]# ls
inventory  playbooks  roles
[root@lb openshift-ansible]# cd play*
[root@lb playbooks]# ls
adhoc                 openshift-etcd          openshift-prometheus
aws                   openshift-glusterfs     openshift-provisioners
byo                   openshift-grafana       openshift-service-catalog
cluster-operator      openshift-hosted        openshift-web-console
common                openshift-loadbalancer  openstack
container-runtime     openshift-logging       prerequisites.yml
deploy_cluster.retry  openshift-management    README.md
deploy_cluster.yml    openshift-master        redeploy-certificates.yml
gcp                   openshift-metrics       roles
init                  openshift-nfs
openshift-checks      openshift-node
[root@lb playbooks]# pwd
/usr/share/ansible/openshift-ansible/playbooks
[root@lb playbooks]# find ./ -name "is different from OCP image"
[root@lb playbooks]# find / -name "is different from OCP image"
^[[A^C
[root@lb playbooks]# find ./ -na^Cis different from OCP image"
[root@lb playbooks]# grep -r "is different from OCP image" .
[root@lb playbooks]# grep -r "is different from OCP image" ./
[root@lb playbooks]# grep -r "is different from OCP image" ./^C
[root@lb playbooks]# cd openshift-checks
[root@lb openshift-checks]# ls
adhoc.yml           health.yml       private    roles
certificate_expiry  pre-install.yml  README.md
[root@lb openshift-checks]# cd pre*
-bash: cd: pre-install.yml: Not a directory
[root@lb openshift-checks]# vi pre*
[root@lb openshift-checks]# ls
adhoc.yml           health.yml       private    roles
certificate_expiry  pre-install.yml  README.md
[root@lb openshift-checks]# vi pre*
[root@lb openshift-checks]# pwd
/usr/share/ansible/openshift-ansible/playbooks/openshift-checks
[root@lb openshift-checks]# cd ..
[root@lb playbooks]# ls
adhoc                 openshift-etcd          openshift-prometheus
aws                   openshift-glusterfs     openshift-provisioners
byo                   openshift-grafana       openshift-service-catalog
cluster-operator      openshift-hosted        openshift-web-console
common                openshift-loadbalancer  openstack
container-runtime     openshift-logging       prerequisites.yml
deploy_cluster.retry  openshift-management    README.md
deploy_cluster.yml    openshift-master        redeploy-certificates.yml
gcp                   openshift-metrics       roles
init                  openshift-nfs
openshift-checks      openshift-node
[root@lb playbooks]# init
init: required argument missing.
[root@lb playbooks]# cd init
[root@lb init]# ls
base_packages.yml  evaluate_groups.yml  roles                   vars
basic_facts.yml    main.yml             sanity_checks.yml       version.yml
cluster_facts.yml  repos.yml            validate_hostnames.yml
[root@lb init]# vi main.yml
[root@lb init]# ls
base_packages.yml  evaluate_groups.yml  roles                   vars
basic_facts.yml    main.yml             sanity_checks.yml       version.yml
cluster_facts.yml  repos.yml            validate_hostnames.yml
[root@lb init]# cd ..
[root@lb playbooks]# ls
adhoc                 openshift-etcd          openshift-prometheus
aws                   openshift-glusterfs     openshift-provisioners
byo                   openshift-grafana       openshift-service-catalog
cluster-operator      openshift-hosted        openshift-web-console
common                openshift-loadbalancer  openstack
container-runtime     openshift-logging       prerequisites.yml
deploy_cluster.retry  openshift-management    README.md
deploy_cluster.yml    openshift-master        redeploy-certificates.yml
gcp                   openshift-metrics       roles
init                  openshift-nfs
openshift-checks      openshift-node
[root@lb playbooks]# pwd
/usr/share/ansible/openshift-ansible/playbooks
[root@lb playbooks]# vi openshif-checks
[root@lb playbooks]# pwd
/usr/share/ansible/openshift-ansible/playbooks
[root@lb playbooks]# ls
adhoc                 openshift-etcd          openshift-prometheus
aws                   openshift-glusterfs     openshift-provisioners
byo                   openshift-grafana       openshift-service-catalog
cluster-operator      openshift-hosted        openshift-web-console
common                openshift-loadbalancer  openstack
container-runtime     openshift-logging       prerequisites.yml
deploy_cluster.retry  openshift-management    README.md
deploy_cluster.yml    openshift-master        redeploy-certificates.yml
gcp                   openshift-metrics       roles
init                  openshift-nfs
openshift-checks      openshift-node
[root@lb playbooks]# cd openshift-check*
[root@lb openshift-checks]# ls
adhoc.yml           health.yml       private    roles
certificate_expiry  pre-install.yml  README.md
[root@lb openshift-checks]# vi pre-install.yml
[root@lb openshift-checks]# pwd
/usr/share/ansible/openshift-ansible/playbooks/openshift-checks
[root@lb openshift-checks]# cd private
[root@lb private]# l
-bash: l: command not found
[root@lb private]# ls
adhoc.yml  health.yml  install.yml  pre-install.yml  roles
[root@lb private]# vi pre-install.yml
[root@lb private]# pwd
/usr/share/ansible/openshift-ansible/playbooks/openshift-checks/private
[root@lb private]# ls
adhoc.yml  health.yml  install.yml  pre-install.yml  roles
[root@lb private]# vi health.yml
[root@lb private]# ls
adhoc.yml  health.yml  install.yml  pre-install.yml  roles
[root@lb private]# find / -type f | xargs grep 'openshift'
grep: /proc/fs/nfsd/unlock_filesystem: Invalid argument
grep: /proc/fs/nfsd/unlock_ip: Invalid argument
grep: /proc/fs/nfsd/filehandle: Invalid argument
grep: /proc/sys/fs/binfmt_misc/register: Invalid argument
grep: /proc/sys/net/ipv4/route/flush: Permission denied
grep: /proc/sys/net/ipv6/conf/all/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/default/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/docker0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/eth0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/lo/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/route/flush: Permission denied
grep: /proc/sys/vm/compact_memory: Permission denied
^C
[root@lb private]# find / -name openshift
/etc/selinux/targeted/active/modules/100/openshift
/etc/logrotate.d/openshift
/etc/tuned/openshift
/root/keycloak-4.8.3.Final/modules/system/layers/keycloak/com/openshift
/var/log/openshift
^C
[root@lb private]# grep -insr "OCP"
[root@lb private]# grep -r -H "OCP"  /
Binary file /boot/initramfs-0-rescue-a116344dea3f495a85a6a272b3b1db38.img matche                                                                             s
Binary file /boot/initramfs-3.10.0-862.el7.x86_64kdump.img matches
Binary file /boot/initramfs-3.10.0-862.el7.x86_64.img matches
Binary file /boot/initramfs-3.10.0-862.3.2.el7.x86_64kdump.img matches
grep: /proc/fs/nfsd/unlock_filesystem: Invalid argument
grep: /proc/fs/nfsd/unlock_ip: Invalid argument
grep: /proc/fs/nfsd/filehandle: Invalid argument
grep: /proc/sys/fs/binfmt_misc/register: Invalid argument
grep: /proc/sys/net/ipv4/route/flush: Permission denied
grep: /proc/sys/net/ipv6/conf/all/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/default/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/docker0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/eth0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/lo/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/route/flush: Permission denied
grep: /proc/sys/vm/compact_memory: Permission denied
^C
[root@lb private]# ack -i "is different from OCP image"  /
-bash: ack: command not found
[root@lb private]# grep -rl 'is different from OCP image' /
grep: /proc/fs/nfsd/unlock_filesystem: Invalid argument
grep: /proc/fs/nfsd/unlock_ip: Invalid argument
grep: /proc/fs/nfsd/filehandle: Invalid argument
grep: /proc/sys/fs/binfmt_misc/register: Invalid argument
grep: /proc/sys/net/ipv4/route/flush: Permission denied
grep: /proc/sys/net/ipv6/conf/all/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/default/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/docker0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/eth0/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/conf/lo/stable_secret: Input/output error
grep: /proc/sys/net/ipv6/route/flush: Permission denied
grep: /proc/sys/vm/compact_memory: Permission denied
^C
[root@lb private]# grep -rl 'is different from OCP image' /usr/share/ansible
/usr/share/ansible/openshift-ansible/roles/openshift_version/tasks/masters_and_n                                                                             odes.yml
[root@lb private]# vi /usr/share/ansible/openshift-ansible/roles/openshift_version/tasks/masters_and_nodes.yml                                               [root@lb private]#
