login as: root
root@192.168.225.175's password:
Last login: Wed Jun  5 01:55:55 2019 from 192.168.225.253
[root@appnode4 ~]# lsblk
NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
fd0             2:0    1    4K  0 disk
sda             8:0    0   50G  0 disk
├─sda1          8:1    0    1G  0 part /boot
└─sda2          8:2    0   49G  0 part
  ├─rhel-root 253:0    0   44G  0 lvm  /
  └─rhel-swap 253:1    0    5G  0 lvm  [SWAP]
sr0            11:0    1 1024M  0 rom
[root@appnode4 ~]# lvs
  LV   VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Co                                                                                nvert
  root rhel -wi-ao---- <44.00g                                                                                                                               
  swap rhel -wi-ao----   5.00g                                                                                                                               
[root@appnode4 ~]# vgs
  VG   #PV #LV #SN Attr   VSize   VFree
  rhel   1   2   0 wz--n- <49.00g    0
[root@appnode4 ~]# pvs
  PV         VG   Fmt  Attr PSize   PFree
  /dev/sda2  rhel lvm2 a--  <49.00g    0
[root@appnode4 ~]# vgremove docker-vg
  Volume group "docker-vg" not found
  Cannot process volume group docker-vg
[root@appnode4 ~]# vgcreate docker-vg /dev/sdb
  Device /dev/sdb not found.
[root@appnode4 ~]# fdisk /dev/sdb
fdisk: cannot open /dev/sdb: No such file or directory
[root@appnode4 ~]# partprobe
[root@appnode4 ~]# fdisk /dev/sdb
fdisk: cannot open /dev/sdb: No such file or directory
[root@appnode4 ~]# vgcreate docker-vg /dev/sdb1
  Device /dev/sdb1 not found.
[root@appnode4 ~]# pwd
/root
[root@appnode4 ~]# cd /etc/sysconfig
[root@appnode4 sysconfig]# ls
anaconda                          ebtables-config   netconsole
atomic                            firewalld         network
authconfig                        grub              network-scripts
cbq                               init              rdisc
chronyd                           ip6tables         readonly-root
console                           ip6tables-config  rhn
cpupower                          iptables          rsyncd
crond                             iptables-config   rsyslog
docker                            irqbalance        run-parts
docker-network                    kdump             selinux
docker-storage                    kernel            sshd
docker-storage-backup20190605-01  man-db            wpa_supplicant
docker-storage-setup              modules
[root@appnode4 sysconfig]# vi docker-storage-setup
[root@appnode4 sysconfig]# fdisk /dev/sdb
fdisk: cannot open /dev/sdb: No such file or directory
[root@appnode4 sysconfig]# vgcreate docker-vg /dev/sdb
  Device /dev/sdb not found.
[root@appnode4 sysconfig]# lsblk
NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
fd0             2:0    1    4K  0 disk
sda             8:0    0   50G  0 disk
├─sda1          8:1    0    1G  0 part /boot
└─sda2          8:2    0   49G  0 part
  ├─rhel-root 253:0    0   44G  0 lvm  /
  └─rhel-swap 253:1    0    5G  0 lvm  [SWAP]
sr0            11:0    1 1024M  0 rom
[root@appnode4 sysconfig]#
Broadcast message from root@appnode4 (Wed 2019-06-05 06:28:45 EDT):

The system is going down for power-off at Wed 2019-06-05 06:29:45 EDT!


Broadcast message from root@appnode4 (Wed 2019-06-05 06:29:07 EDT):

The system is going down for power-off at Wed 2019-06-05 06:30:07 EDT!


Broadcast message from root@appnode4 (Wed 2019-06-05 06:29:15 EDT):

The system is going down for power-off at Wed 2019-06-05 06:30:15 EDT!


Broadcast message from root@appnode4 (Wed 2019-06-05 06:29:33 EDT):

The system is going down for power-off at Wed 2019-06-05 06:30:33 EDT!
