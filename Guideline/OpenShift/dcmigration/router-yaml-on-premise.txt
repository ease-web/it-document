apiVersion: v1
kind: DeploymentConfig
metadata:
  name: router
  namespace: default
  selfLink: /oapi/v1/namespaces/default/deploymentconfigs/router
  uid: 99859122-2cfb-11e7-b8d6-00155dde16b4
  resourceVersion: '34776735'
  generation: 9
  creationTimestamp: '2017-04-29T16:47:58Z'
  labels:
    router: router
spec:
  strategy:
    type: Rolling
    rollingParams:
      updatePeriodSeconds: 1
      intervalSeconds: 1
      timeoutSeconds: 600
      maxUnavailable: 25%
      maxSurge: 0
    resources: {}
    activeDeadlineSeconds: 21600
  triggers:
    - type: ConfigChange
  replicas: 1
  test: false
  selector:
    router: router
  template:
    metadata:
      creationTimestamp: null
      labels:
        router: router
    spec:
      volumes:
        - name: server-certificate
          secret:
            secretName: router-certs
            defaultMode: 420
      containers:
        - name: router
          image: 'openshift3/ose-haproxy-router:v3.5.5.8'
          ports:
            - hostPort: 80
              containerPort: 80
              protocol: TCP
            - hostPort: 443
              containerPort: 443
              protocol: TCP
            - name: stats
              hostPort: 1936
              containerPort: 1936
              protocol: TCP
          env:
            - name: DEFAULT_CERTIFICATE_DIR
              value: /etc/pki/tls/private
            - name: ROUTER_EXTERNAL_HOST_HOSTNAME
            - name: ROUTER_EXTERNAL_HOST_HTTPS_VSERVER
            - name: ROUTER_EXTERNAL_HOST_HTTP_VSERVER
            - name: ROUTER_EXTERNAL_HOST_INSECURE
              value: 'false'
            - name: ROUTER_EXTERNAL_HOST_INTERNAL_ADDRESS
            - name: ROUTER_EXTERNAL_HOST_PARTITION_PATH
            - name: ROUTER_EXTERNAL_HOST_PASSWORD
            - name: ROUTER_EXTERNAL_HOST_PRIVKEY
              value: /etc/secret-volume/router.pem
            - name: ROUTER_EXTERNAL_HOST_USERNAME
            - name: ROUTER_EXTERNAL_HOST_VXLAN_GW_CIDR
            - name: ROUTER_SERVICE_HTTPS_PORT
              value: '443'
            - name: ROUTER_SERVICE_HTTP_PORT
              value: '80'
            - name: ROUTER_SERVICE_NAME
              value: router
            - name: ROUTER_SERVICE_NAMESPACE
              value: default
            - name: ROUTER_SUBDOMAIN
            - name: STATS_PASSWORD
              value: Os8lrgXCv5
            - name: STATS_PORT
              value: '1936'
            - name: STATS_USERNAME
              value: admin
            - name: ROUTER_LOG_LEVEL
              value: debug
            - name: ROUTER_SYSLOG_ADDRESS
          resources:
            requests:
              cpu: 100m
              memory: 256Mi
          volumeMounts:
            - name: server-certificate
              readOnly: true
              mountPath: /etc/pki/tls/private
          livenessProbe:
            httpGet:
              path: /healthz
              port: 1936
              host: localhost
              scheme: HTTP
            initialDelaySeconds: 10
            timeoutSeconds: 1
            periodSeconds: 10
            successThreshold: 1
            failureThreshold: 3
          readinessProbe:
            httpGet:
              path: /healthz
              port: 1936
              host: localhost
              scheme: HTTP
            initialDelaySeconds: 10
            timeoutSeconds: 1
            periodSeconds: 10
            successThreshold: 1
            failureThreshold: 3
          terminationMessagePath: /dev/termination-log
          imagePullPolicy: IfNotPresent
      restartPolicy: Always
      terminationGracePeriodSeconds: 30
      dnsPolicy: ClusterFirst
      nodeSelector:
        region: infra
      serviceAccountName: router
      serviceAccount: router
      hostNetwork: true
      securityContext: {}
status:
  latestVersion: 4
  observedGeneration: 9
  replicas: 1
  updatedReplicas: 1
  availableReplicas: 1
  unavailableReplicas: 0
  details:
    message: config change
    causes:
      - type: ConfigChange
  conditions:
    - type: Progressing
      status: 'True'
      lastUpdateTime: '2017-05-03T02:56:28Z'
      lastTransitionTime: '2017-05-03T02:56:23Z'
      reason: NewReplicationControllerAvailable
      message: replication controller "router-4" successfully rolled out
    - type: Available
      status: 'True'
      lastUpdateTime: '2019-04-04T11:15:46Z'
      lastTransitionTime: '2019-04-04T11:15:46Z'
      message: Deployment config has minimum availability.
  readyReplicas: 1
