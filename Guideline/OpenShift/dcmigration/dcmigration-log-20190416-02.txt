login as: root
root@192.168.222.212's password:
Last failed login: Thu Apr 11 17:59:54 HKT 2019 from 192.168.223.105 on ssh:nott                                                                             y
There were 2 failed login attempts since the last successful login.
Last login: Thu Apr 11 16:03:05 2019 from 192.168.223.159
[root@vm-OpenShift-POC-1 ~]# cd /etc
[root@vm-OpenShift-POC-1 etc]# ls
abrt                        gshadow-                   profile
adjtime                     gss                        profile.d
aliases                     gssproxy                   protocols
aliases.db                  host.conf                  pulse
alsa                        hostname                   purple
alternatives                hosts                      python
anacrontab                  hosts.allow                qemu-ga
ansible                     hosts.deny                 qemu-kvm
asound.conf                 hp                         radvd.conf
at.deny                     idmapd.conf                ras
at-spi2                     init.d                     rc0.d
audisp                      inittab                    rc1.d
audit                       inputrc                    rc2.d
avahi                       iproute2                   rc3.d
bash_completion.d           ipsec.conf                 rc4.d
bashrc                      ipsec.d                    rc5.d
binfmt.d                    ipsec.secrets              rc6.d
bluetooth                   iscsi                      rc.d
bonobo-activation           issue                      rc.local
brltty                      issue.net                  rdma
brltty.conf                 java                       redhat-access-insights
ceph                        jvm                        redhat-lsb
cgconfig.conf               jvm-commmon                redhat-release
cgconfig.d                  kdump.conf                 request-key.conf
cgrules.conf                kernel                     request-key.d
cgsnapshot_blacklist.conf   krb5.conf                  resolv.conf
chkconfig.d                 krb5.conf.d                rhsm
chrony.conf                 ksmtuned.conf              rpc
chrony.keys                 ld.so.cache                rpm
cifs-utils                  ld.so.conf                 rsyncd.conf
cni                         ld.so.conf.d               rsyslog.conf
cockpit                     libaudit.conf              rsyslog.d
conntrackd                  libibverbs.d               rwtab
containers                  libnl                      rwtab.d
cron.d                      libreport                  samba
cron.daily                  libuser.conf               sane.d
cron.deny                   libvirt                    sasl2
cron.hourly                 locale.conf                scl
cron.monthly                localtime                  securetty
crontab                     login.defs                 security
cron.weekly                 logrotate.conf             selinux
crypttab                    logrotate.d                services
csh.cshrc                   lsb-release.d              sestatus.conf
csh.login                   lsm                        setroubleshoot
cups                        lvm                        setuptool.d
cupshelpers                 machine-id                 sgml
dbus-1                      magic                      shadow
dconf                       mail.rc                    shadow-
default                     makedumpfile.conf.sample   shells
depmod.d                    man_db.conf                skel
dhcp                        maven                      smartmontools
DIR_COLORS                  mcelog                     sos.conf
DIR_COLORS.256color         mke2fs.conf                sound
DIR_COLORS.lightbgcolor     modprobe.d                 speech-dispatcher
dleyna-server-service.conf  modules-load.d             ssh
dnsmasq.conf                motd                       ssl
dnsmasq.d                   mtab                       statetab
docker                      mtools.conf                statetab.d
dracut.conf                 multipath                  subgid
dracut.conf.d               my.cnf                     subuid
drirc                       my.cnf.d                   sudo.conf
e2fsck.conf                 nanorc                     sudoers
enscript.cfg                netconfig                  sudoers.d
environment                 NetworkManager             sudo-ldap.conf
etcd                        networks                   sysconfig
ethertypes                  nfsmount.conf              sysctl.conf
exports                     nsswitch.conf              sysctl.d
exports.d                   nsswitch.conf.bak          systemd
favicon.png                 ntp                        system-release
fcoe                        numad.conf                 system-release-cpe
festival                    oci-register-machine.conf  tcsd.conf
filesystems                 oddjob                     terminfo
firefox                     oddjobd.conf               tmpfiles.d
firewalld                   oddjobd.conf.d             trusted-key.key
fonts                       openldap                   tuned
fprintd.conf                openvswitch                udev
fstab                       opt                        udisks2
fuse.conf                   origin                     unbound
gconf                       os-release                 updatedb.conf
gcrypt                      PackageKit                 UPower
gdbinit                     pam.d                      usb_modeswitch.conf
gdbinit.d                   passwd                     vconsole.conf
gdm                         passwd-                    vimrc
geoclue                     pbm2ppa.conf               virc
GeoIP.conf                  pinforc                    vmware-tools
GeoIP.conf.default          pkcs11                     wgetrc
ghostscript                 pki                        wpa_supplicant
gnome-vfs-2.0               plymouth                   wvdial.conf
gnupg                       pm                         X11
GREP_COLORS                 pnm2ppa.conf               xdg
groff                       polkit-1                   xinetd.d
group                       popt.d                     xml
group-                      postfix                    yum
grub2.cfg                   ppp                        yum.conf
grub.d                      prelink.conf.d             yum.repos.d
gshadow                     printcap
[root@vm-OpenShift-POC-1 etc]# vi yum.conf
[root@vm-OpenShift-POC-1 etc]# cd ssl
[root@vm-OpenShift-POC-1 ssl]# ls
certs
[root@vm-OpenShift-POC-1 ssl]# cd certs
[root@vm-OpenShift-POC-1 certs]# ls
ca-bundle.crt  ca-bundle.trust.crt  make-dummy-cert  Makefile  renew-dummy-cert
[root@vm-OpenShift-POC-1 certs]# ls -al
total 12
drwxr-xr-x. 2 root root  117 Apr 27  2017 .
drwxr-xr-x. 5 root root   81 Apr 27  2017 ..
lrwxrwxrwx. 1 root root   49 Apr 27  2017 ca-bundle.crt -> /etc/pki/ca-trust/ext                                                                             racted/pem/tls-ca-bundle.pem
lrwxrwxrwx. 1 root root   55 Apr 27  2017 ca-bundle.trust.crt -> /etc/pki/ca-tru                                                                             st/extracted/openssl/ca-bundle.trust.crt
-rwxr-xr-x. 1 root root  610 Sep 22  2016 make-dummy-cert
-rw-r--r--. 1 root root 2388 Sep 22  2016 Makefile
-rwxr-xr-x. 1 root root  829 Sep 22  2016 renew-dummy-cert
[root@vm-OpenShift-POC-1 certs]# cd ..
[root@vm-OpenShift-POC-1 ssl]# ls
certs
[root@vm-OpenShift-POC-1 ssl]# pwd
/etc/ssl
[root@vm-OpenShift-POC-1 ssl]# cd ..
[root@vm-OpenShift-POC-1 etc]# cd ssh
[root@vm-OpenShift-POC-1 ssh]# ls
moduli       ssh_host_ecdsa_key      ssh_host_ed25519_key.pub
ssh_config   ssh_host_ecdsa_key.pub  ssh_host_rsa_key
sshd_config  ssh_host_ed25519_key    ssh_host_rsa_key.pub
[root@vm-OpenShift-POC-1 ssh]# cd ../ssl
[root@vm-OpenShift-POC-1 ssl]# ls
certs
[root@vm-OpenShift-POC-1 ssl]# cd certs
[root@vm-OpenShift-POC-1 certs]# ls
ca-bundle.crt  ca-bundle.trust.crt  make-dummy-cert  Makefile  renew-dummy-cert
[root@vm-OpenShift-POC-1 certs]# ls -al
total 12
drwxr-xr-x. 2 root root  117 Apr 27  2017 .
drwxr-xr-x. 5 root root   81 Apr 27  2017 ..
lrwxrwxrwx. 1 root root   49 Apr 27  2017 ca-bundle.crt -> /etc/pki/ca-trust/ext                                                                             racted/pem/tls-ca-bundle.pem
lrwxrwxrwx. 1 root root   55 Apr 27  2017 ca-bundle.trust.crt -> /etc/pki/ca-tru                                                                             st/extracted/openssl/ca-bundle.trust.crt
-rwxr-xr-x. 1 root root  610 Sep 22  2016 make-dummy-cert
-rw-r--r--. 1 root root 2388 Sep 22  2016 Makefile
-rwxr-xr-x. 1 root root  829 Sep 22  2016 renew-dummy-cert
[root@vm-OpenShift-POC-1 certs]# https://maam-dev.axa.com/maam/v2/jwks
-bash: https://maam-dev.axa.com/maam/v2/jwks: No such file or directory
[root@vm-OpenShift-POC-1 certs]# curl https://maam-dev.axa.com/maam/v2/jwks
{
  "keys": [
    {
      "kid": "IdT20180306",
      "kty": "RSA",
      "e": "AQAB",
      "use": "sig",
      "alg": "RS256",
      "n": "0Y2yJaA_nHMwwl8uPKp_KTVxEYjcLahcS_8WbSHfyypNbpwcQE27-6VKs5q9yc1Tw7pt                                                                             LPxfPZ8L1naOriK-QzgbiXtmGf75imbWWdxI92DWn1asi5ggyxWrOQtt791e4oaxzZNCCebVkCRZAUvO                                                                             KLHEptBrHJbZkUur5a1dsXt8FeHecWrEQ2l44JnGLFmO33kFWDUH_q4Kfujp-LzLvDTNZQFycMUhE3Gn                                                                             l-wxqQrxQmtjZ6B0mHNZM1ihdk_NchFD7dD2tpZFgo4Tj41aJKZlL1_ECIQ7BjKfLUZthTSN8yJc1Xzi                                                                             CtPgJuJ3BHO7wd99VsgOBrVoZJMtjzZVKw"
    },
    {
      "kty": "RSA",
      "e": "AQAB",
      "use": "sig",
      "kid": "AT20180423",
      "alg": "RS256",
      "n": "irrSk57z-sh1cH98zyv-lXs7PvyDKaHZzw_6y2Nhi0hTJNVixboDRdkNGF1U2O7vWhzg                                                                             IzBmnMgqsTCw13bKAk4J9GFR4CYZvO3HfHUQiTkcOd5lEbvHgUeZ3HxlQFPWRxtYKxsFtMDIM_mr8iO3                                                                             3DFIRzT_mdjMTczkUVEl9hINW-NGI8QahX2EgPg2uvxWIv5PBdqn97mcF5xpDWDv8xghWEEODsxlhkQK                                                                             85040WcNv8A7IxWzvtyNhCce3EFB8x4N5MZaD2UR5CAHz_YieIlJfzNyn__Ku74kLmP-_1vioMOhh2pz                                                                             R55AoSBsOHtfHECcdPN9aTY1y0Qje6y3cQ"
    }
  ]
}[root@vm-OpenShift-POC-1 certs]# curl https://maam-dev.axa.com/maam/v2/jwks
{
  "keys": [
    {
      "kid": "IdT20180306",
      "kty": "RSA",
      "e": "AQAB",
      "use": "sig",
      "alg": "RS256",
      "n": "0Y2yJaA_nHMwwl8uPKp_KTVxEYjcLahcS_8WbSHfyypNbpwcQE27-6VKs5q9yc1Tw7pt                                                                             LPxfPZ8L1naOriK-QzgbiXtmGf75imbWWdxI92DWn1asi5ggyxWrOQtt791e4oaxzZNCCebVkCRZAUvO                                                                             KLHEptBrHJbZkUur5a1dsXt8FeHecWrEQ2l44JnGLFmO33kFWDUH_q4Kfujp-LzLvDTNZQFycMUhE3Gn                                                                             l-wxqQrxQmtjZ6B0mHNZM1ihdk_NchFD7dD2tpZFgo4Tj41aJKZlL1_ECIQ7BjKfLUZthTSN8yJc1Xzi                                                                             CtPgJuJ3BHO7wd99VsgOBrVoZJMtjzZVKw"
    },
    {
      "kty": "RSA",
      "e": "AQAB",
      "use": "sig",
      "kid": "AT20180423",
      "alg": "RS256",
      "n": "irrSk57z-sh1cH98zyv-lXs7PvyDKaHZzw_6y2Nhi0hTJNVixboDRdkNGF1U2O7vWhzg                                                                             IzBmnMgqsTCw13bKAk4J9GFR4CYZvO3HfHUQiTkcOd5lEbvHgUeZ3HxlQFPWRxtYKxsFtMDIM_mr8iO3                                                                             3DFIRzT_mdjMTczkUVEl9hINW-NGI8QahX2EgPg2uvxWIv5PBdqn97mcF5xpDWDv8xghWEEODsxlhkQK                                                                             85040WcNv8A7IxWzvtyNhCce3EFB8x4N5MZaD2UR5CAHz_YieIlJfzNyn__Ku74kLmP-_1vioMOhh2pz                                                                             R55AoSBsOHtfHECcdPN9aTY1y0Qje6y3cQ"
    }
  ]
}[root@vm-OpenShift-POC-1 certs]#
