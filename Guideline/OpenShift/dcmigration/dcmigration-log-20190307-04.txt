login as: root
root@admin.ocp.eabdc's password:
Last login: Thu Mar  7 10:13:51 2019 from 192.168.225.253
[root@lb ~]# oc login https://admin.ocp.eabdc:8443 --token=jYim52ligxwIdcpV85tKIYlf9a4fBbiNZhZ9DER5SMQ
Logged into "https://admin.ocp.eabdc:8443" as "admin" using the token provided.

You have access to the following projects and can switch between them with 'oc project <projectname>':

    axasgsit
    axasgsit2
  * axasgsit3
    default
    demo2
    eab
    kube-public
    kube-service-catalog
    kube-system
    logging
    management-infra
    openshift
    openshift-infra
    openshift-metrics
    openshift-node
    openshift-web-console
    php
    sftp
    test
    timesheet

Using project "axasgsit3".
[root@lb ~]# oc set env dc/ease-admin --overwrite INTERNET_PROXY=""
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite INTRANET_PROXY=""
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite INTERNET_PROXY_HOSTS="{\"hostname\": [\"preprodapwsg.axa-tech.com:10443\", \"maam-dev.axa.com\", \"api.infinite-convergence.com\"]}"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite INTRANET_PROXY_HOSTS="{\"hostname\": [\"ease-sit.intranet\", \"ease-sit2.intranet\"]}"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite WEB_API_MAAM="https://maam-dev.axa.com/dev/token"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite MAAM_USERNAME="866279d0-cd4e-4211-a4b7-85c3a707346a"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite MAAM_SECRET="3651852b-3506-4c78-bcf5-e19784c516ee"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite GATEWAY_URL="http://192.168.222.97"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite GATEWAY_PORT="4984"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite GATEWAY_DBNAME="axasgsit"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite GATEWAY_USER="easeWeb"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite GATEWAY_PW="ABC1234"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite WEB_API_SMTP_HOST="192.168.222.127"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite WEB_API_SMTP_PORT="25"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite WEB_JDBC_URL="jdbc:oracle:thin:@192.168.222.156:1521:axasgsit"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite WEB_JDBC_USERNAME="epos_sit2"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite WEB_JDBC_PASSWORD="password"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite ADMIN_JDBC_URL="jdbc:oracle:thin:@192.168.222.156:1521:axasgsit"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite ADMIN_JDBC_USERNAME="eadmin_sit2"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite ADMIN_JDBC_PASSWORD="password"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite ADMIN_SAML_ENTRY_POINT=""
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite ADMIN_SAML_LOGOUT=""
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]# oc set env dc/ease-admin --overwrite INTERNAL_API_DOMAIN="http://ease-api:8080/ease-api"
deploymentconfig.apps.openshift.io/ease-admin updated
[root@lb ~]#
