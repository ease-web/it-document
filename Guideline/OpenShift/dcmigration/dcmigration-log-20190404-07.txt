login as: root
root@192.168.225.121's password:
Last login: Thu Apr  4 05:19:03 2019 from 192.168.225.253
[root@appnode2 ~]# oc login https://admin.ocp.eabdc:8443 --token=4LjBjmlTFVVynci                                                                             EAM5CqKxS_PzaOXHpr-5ILhxKULI
Logged into "https://admin.ocp.eabdc:8443" as "admin" using the token provided.

You have access to the following projects and can switch between them with 'oc p                                                                             roject <projectname>':

    121demo
    ax-project-001
    axasgsit
    axasgsit2
    axasgsit3
    default
    demo2
    eab
    eab-121-v3-dev
    eab-cafe-dev
    eab-cafe-sit
    eab-ocr-service
  * eab-time-sheet
    eab-time-sheet-dev
    eabtimesheet
    kube-public
    kube-service-catalog
    kube-system
    logging
    management-infra
    ocr-service-dev
    ocr-service-sit
    openshift
    openshift-infra
    openshift-metrics
    openshift-node
    openshift-web-console
    php
    sftp
    test

Using project "eab-time-sheet".
[root@appnode2 ~]# oc adm manage-node appnode2.ocp.eabdc --schedulable=false
NAME                 STATUS                     ROLES     AGE       VERSION
appnode2.ocp.eabdc   Ready,SchedulingDisabled   compute   313d      v1.9.1+a0ce1                                                                             bc657
[root@appnode2 ~]# systemctl stop docker atomic-openshift-node
[root@appnode2 ~]# rm -rf /var/lib/origin/openshift.local.volumes
rm: cannot remove ‘/var/lib/origin/openshift.local.volumes/pods/82f4bf04-603e-11                                                                             e8-91c6-00155ddeb901/volumes/kubernetes.io~secret/prometheus-node-exporter-token                                                                             -fndbl’: Device or resource busy
rm: cannot remove ‘/var/lib/origin/openshift.local.volumes/pods/6b65bb2c-6147-11                                                                             e8-bfb2-00155ddeb903/volumes/kubernetes.io~secret/aggregated-logging-fluentd-tok                                                                             en-plpsb’: Device or resource busy
rm: cannot remove ‘/var/lib/origin/openshift.local.volumes/pods/6b65bb2c-6147-11                                                                             e8-bfb2-00155ddeb903/volumes/kubernetes.io~secret/certs’: Device or resource bus                                                                             y
[root@appnode2 ~]# rm -rf /var/lib/origin/openshift.local.volumes
rm: cannot remove ‘/var/lib/origin/openshift.local.volumes/pods/82f4bf04-603e-11e8-91c6-00155ddeb901/volumes/kubernetes.io~secret/prometheus-node-exporter-token-fndbl’: Device or resource busy
rm: cannot remove ‘/var/lib/origin/openshift.local.volumes/pods/6b65bb2c-6147-11e8-bfb2-00155ddeb903/volumes/kubernetes.io~secret/aggregated-logging-fluentd-token-plpsb’: Device or resource busy
rm: cannot remove ‘/var/lib/origin/openshift.local.volumes/pods/6b65bb2c-6147-11e8-bfb2-00155ddeb903/volumes/kubernetes.io~secret/certs’: Device or resource busy
[root@appnode2 ~]# umount /var/lib/origin/openshift.local.volumes/pods/82f4bf04-603e-11e8-91c6-00155ddeb901/volumes/kubernetes.io~secret/prometheus-node-exporter-token-fndbl
umount /var/lib/origin/openshift.local.volumes/pods/6b65bb2c-6147-11e8-bfb2-00155ddeb903/volumes/kubernetes.io~secret/aggregated-logging-fluentd-token-plpsb
umount /var/lib/origin/openshift.local.volumes/pods/6b65bb2c-6147-11e8-bfb2-00155ddeb903/volumes/kubernetes.io~secret/certs
[root@appnode2 ~]# umount /var/lib/origin/openshift.local.volumes/pods/6b65bb2c-6147-11e8-bfb2-00155ddeb903/volumes/kubernetes.io~secret/aggregated-logging-fluentd-token-plpsb
[root@appnode2 ~]# umount /var/lib/origin/openshift.local.volumes/pods/6b65bb2c-6147-11e8-bfb2-00155ddeb903/volumes/kubernetes.io~secret/certs
[root@appnode2 ~]# rm -rf /var/lib/origin/openshift.local.volumes                                                                                            [root@appnode2 ~]# rm -rf /var/lib/docker
[root@appnode2 ~]# docker-storage-setup --reset
  Logical volume "docker-pool" successfully removed
[root@appnode2 ~]# docker-storage-setup
INFO: Device /dev/sdb is already partitioned and is part of volume group docker-                                                                             vg
  Rounding up size to full physical extent 52.00 MiB
  Thin pool volume with chunk size 512.00 KiB can address at most 126.50 TiB of                                                                              data.
  Logical volume "docker-pool" created.
  Logical volume docker-vg/docker-pool changed.
[root@appnode2 ~]# mkdir /var/lib/docker
[root@appnode2 ~]# systemctl start docker atomic-openshift-node
[root@appnode2 ~]# oc adm manage-node appnode2.ocp.eabdc --schedulable=true
NAME                 STATUS    ROLES     AGE       VERSION
appnode2.ocp.eabdc   Ready     compute   313d      v1.9.1+a0ce1bc657
[root@appnode2 ~]# oc get nodes
NAME                   STATUS                        ROLES     AGE       VERSION
appnode1.ocp.eabdc     Ready                         compute   313d      v1.9.1+                                                                             a0ce1bc657
appnode2.ocp.eabdc     Ready                         compute   313d      v1.9.1+                                                                             a0ce1bc657
appnode3.ocp.eabdc     NotReady,SchedulingDisabled   compute   313d      v1.9.1+                                                                             a0ce1bc657
infranode1.ocp.eabdc   Ready                         infra     313d      v1.9.1+                                                                             a0ce1bc657
infranode2.ocp.eabdc   Ready                         infra     313d      v1.9.1+                                                                             a0ce1bc657
infranode3.ocp.eabdc   Ready                         infra     313d      v1.9.1+                                                                             a0ce1bc657
mnode1.ocp.eabdc       Ready                         master    313d      v1.9.1+                                                                             a0ce1bc657
mnode2.ocp.eabdc       Ready                         master    313d      v1.9.1+                                                                             a0ce1bc657
mnode3.ocp.eabdc       Ready                         master    313d      v1.9.1+                                                                             a0ce1bc657
[root@appnode2 ~]# oc get nodes
NAME                   STATUS                        ROLES     AGE       VERSION
appnode1.ocp.eabdc     Ready                         compute   313d      v1.9.1+                                                                             a0ce1bc657
appnode2.ocp.eabdc     Ready                         compute   313d      v1.9.1+                                                                             a0ce1bc657
appnode3.ocp.eabdc     NotReady,SchedulingDisabled   compute   313d      v1.9.1+                                                                             a0ce1bc657
infranode1.ocp.eabdc   Ready                         infra     313d      v1.9.1+                                                                             a0ce1bc657
infranode2.ocp.eabdc   Ready                         infra     313d      v1.9.1+                                                                             a0ce1bc657
infranode3.ocp.eabdc   Ready                         infra     313d      v1.9.1+                                                                             a0ce1bc657
mnode1.ocp.eabdc       Ready                         master    313d      v1.9.1+                                                                             a0ce1bc657
mnode2.ocp.eabdc       Ready                         master    313d      v1.9.1+                                                                             a0ce1bc657
mnode3.ocp.eabdc       Ready                         master    313d      v1.9.1+                                                                             a0ce1bc657
[root@appnode2 ~]#
