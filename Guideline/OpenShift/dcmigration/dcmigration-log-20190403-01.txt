login as: root
root@admin.ocp.eabdc's password:
Last login: Tue Apr  2 18:17:58 2019 from 192.168.225.253
[root@lb ~]# oc login https://admin.ocp.eabdc:8443 --token=pKEQ9DZGyxHV_xVpNIaog                                                                             0YIil2TrDkz40ubx77JkYM
Logged into "https://admin.ocp.eabdc:8443" as "admin" using the token provided.

You have access to the following projects and can switch between them with 'oc p                                                                             roject <projectname>':

    121demo
    ax-project-001
    axasgsit
    axasgsit2
    axasgsit3
  * default
    demo2
    eab
    eab-121-v3-dev
    eab-cafe-dev
    eab-cafe-sit
    eab-ocr-service
    eab-time-sheet
    eab-time-sheet-dev
    eabtimesheet
    kube-public
    kube-service-catalog
    kube-system
    logging
    management-infra
    ocr-service-dev
    ocr-service-sit
    openshift
    openshift-infra
    openshift-metrics
    openshift-node
    openshift-web-console
    php
    sftp
    test

Using project "default".
[root@lb ~]# oc get nodes
NAME                   STATUS    ROLES     AGE       VERSION
appnode1.ocp.eabdc     Ready     compute   312d      v1.9.1+a0ce1bc657
appnode2.ocp.eabdc     Ready     compute   312d      v1.9.1+a0ce1bc657
appnode3.ocp.eabdc     Ready     compute   312d      v1.9.1+a0ce1bc657
infranode1.ocp.eabdc   Ready     infra     312d      v1.9.1+a0ce1bc657
infranode2.ocp.eabdc   Ready     infra     312d      v1.9.1+a0ce1bc657
infranode3.ocp.eabdc   Ready     infra     312d      v1.9.1+a0ce1bc657
mnode1.ocp.eabdc       Ready     master    312d      v1.9.1+a0ce1bc657
mnode2.ocp.eabdc       Ready     master    312d      v1.9.1+a0ce1bc657
mnode3.ocp.eabdc       Ready     master    312d      v1.9.1+a0ce1bc657
[root@lb ~]# oc get events
No resources found.
[root@lb ~]# df -h
Filesystem                 Size  Used Avail Use% Mounted on
/dev/mapper/rhel-root       44G  3.9G   41G   9% /
devtmpfs                   3.9G     0  3.9G   0% /dev
tmpfs                      3.9G     0  3.9G   0% /dev/shm
tmpfs                      3.9G  8.8M  3.9G   1% /run
tmpfs                      3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/sda1                 1014M  184M  831M  19% /boot
/dev/mapper/nfs--vg-lvnfs  200G   67G  134G  34% /nfs
tmpfs                      782M     0  782M   0% /run/user/0
[root@lb ~]# docker images
REPOSITORY              TAG                 IMAGE ID            CREATED                                                                                       SIZE
docker.io/hello-world   latest              fce289e99eb9        3 months ago                                                                                  1.84 kB
[root@lb ~]# oc login https://admin.ocp.eabdc:8443 --token=eyJhbGciOiJSUzI1NiIsInR5                                                                          cCI6IkpXVCJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZX                                                                          J2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJheGFzZ3NpdDMiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2Nvd                                                                          W50L3NlY3JldC5uYW1lIjoiYXhhc2d1c2VyLXRva2VuLWQ2NHpqIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNl                                                                          YWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImF4YXNndXNlciIsImt1YmVybmV0ZXMuaW8vc2Vydml                                                                          jZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6ImFjYzhhZjliLTU1MmMtMTFlOS1iNDA2LTAwMTU1ZG                                                                          RlYjkwMyIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpheGFzZ3NpdDM6YXhhc2d1c2VyIn0.cGLJo                                                                          wgvbGcoggAsk62FZbHMQNPkK-IhHH4hd9f07FEln21DcuFFg2VmghSUWok2RIJP3Czn2aksRTLSOo8qH86v                                                                          pebRMGI76OqSQUu6HHj9IH6eGWTbsnjYqwC7I1Z0y2iwFXD3O4jRYSqVe5nphKssLGK8Ka-FCjD6TxvbVa2                                                                          IzHImMH42oIH5BcaC06kqfXPxTIkiu4DgaFAA2c0uZp22vmtQkcQ_zEknGut7u0E9gAw5HWrWaJX8N_TauK                                                                          5pTSBhAn9zuLJivVMud22IVPApmXTHrcbiRUdGguX-2AR-5BHivdudmm-SKyqiKae5O3LF14MztVBAv174C                                                                          1Ts3g
Logged into "https://admin.ocp.eabdc:8443" as "system:serviceaccount:axasgsit3:axas                                                                          guser" using the token provided.

You have one project on this server: "axasgsit3"

Using project "axasgsit3".
[root@lb ~]# docker login -u admin -p $(oc whoami -t) docker-registry-default.ocp.e                                                                          abdc
Error response from daemon: Get https://docker-registry-default.ocp.eabdc/v1/users/                                                                          : x509: certificate signed by unknown authority
[root@lb ~]# ls
anaconda-ks.cfg
ax
keycloak-4.8.3.Final
keycloak-4.8.3.Final.tar.gz
oc-3.11.59-linux.tar.gz
playbook
rhel-7-fast-datapath-rpms.tgz
sat-6-isos--rhel-7-server-ose-3_DOT_9-x86_64-2018-04-30T06.10-01.iso
[root@lb ~]# oc login https://admin.ocp.eabdc:8443 --token=YSScr0lLYJcqW3jm1vXsGvIO                                                                          SVj8s3FE6VrnnYnj4KM
Logged into "https://admin.ocp.eabdc:8443" as "admin" using the token provided.

You have access to the following projects and can switch between them with 'oc proj                                                                          ect <projectname>':

    121demo
    ax-project-001
    axasgsit
    axasgsit2
  * axasgsit3
    default
    demo2
    eab
    eab-121-v3-dev
    eab-cafe-dev
    eab-cafe-sit
    eab-ocr-service
    eab-time-sheet
    eab-time-sheet-dev
    eabtimesheet
    kube-public
    kube-service-catalog
    kube-system
    logging
    management-infra
    ocr-service-dev
    ocr-service-sit
    openshift
    openshift-infra
    openshift-metrics
    openshift-node
    openshift-web-console
    php
    sftp
    test

Using project "axasgsit3".
[root@lb ~]# docker login -u admin -p $(oc whoami -t) docker-registry-default.ocp.e                                                                          abdc
Error response from daemon: Get https://docker-registry-default.ocp.eabdc/v1/users/                                                                          : x509: certificate signed by unknown authority
[root@lb ~]# docker login http://docker-registry-default.ocp.eabdc -u admin -p $(oc whoami                                                                   -t)
Error response from daemon: Get https://docker-registry-default.ocp.eabdc/v1/users/: x509:                                                                   certificate signed by unknown authority
[root@lb ~]# docker login http://docker-registry-default.ocp.eabdc
Username: admin
Password:
Error response from daemon: Get https://docker-registry-default.ocp.eabdc/v1/users/: x509:                                                                   certificate signed by unknown authority
[root@lb ~]# docker login https://docker-registry-default.ocp.eabdc -u admin -p $(oc whoami                                                                   -t)
Error response from daemon: Get https://docker-registry-default.ocp.eabdc/v1/users/: x509:                                                                   certificate signed by unknown authority
[root@lb ~]# docker --insecure-registry=docker-registry-default.ocp.eabdc login https://doc                                                                  ker-registry-default.ocp.eabdc -u admin -p $(oc whoami -t)
unknown flag: --insecure-registry
[root@lb ~]# oc whoami -t
YSScr0lLYJcqW3jm1vXsGvIOSVj8s3FE6VrnnYnj4KM
[root@lb ~]# docker --help

Usage:  docker COMMAND

A self-sufficient runtime for containers

Options:
      --config string      Location of client config files (default "/root/.docker")
  -D, --debug              Enable debug mode
      --help               Print usage
  -H, --host list          Daemon socket(s) to connect to (default [])
  -l, --log-level string   Set the logging level ("debug", "info", "warn", "error", "fatal") (default "info")
      --tls                Use TLS; implied by --tlsverify
      --tlscacert string   Trust certs signed only by this CA (default "/root/.docker/ca.pem")
      --tlscert string     Path to TLS certificate file (default "/root/.docker/cert.pem")
      --tlskey string      Path to TLS key file (default "/root/.docker/key.pem")
      --tlsverify          Use TLS and verify the remote
  -v, --version            Print version information and quit

Management Commands:
  container   Manage containers
  image       Manage images
  network     Manage networks
  node        Manage Swarm nodes
  plugin      Manage plugins
  secret      Manage Docker secrets
  service     Manage services
  stack       Manage Docker stacks
  swarm       Manage Swarm
  system      Manage Docker
  volume      Manage volumes

Commands:
  attach      Attach to a running container
  build       Build an image from a Dockerfile
  commit      Create a new image from a container's changes
  cp          Copy files/folders between a container and the local filesystem
  create      Create a new container
  diff        Inspect changes on a container's filesystem
  events      Get real time events from the server
  exec        Run a command in a running container
  export      Export a container's filesystem as a tar archive
  history     Show the history of an image
  images      List images
  import      Import the contents from a tarball to create a filesystem image
  info        Display system-wide information
  inspect     Return low-level information on Docker objects
  kill        Kill one or more running containers
  load        Load an image from a tar archive or STDIN
  login       Log in to a Docker registry
  logout      Log out from a Docker registry
  logs        Fetch the logs of a container
  pause       Pause all processes within one or more containers
  port        List port mappings or a specific mapping for the container
  ps          List containers
  pull        Pull an image or a repository from a registry
  push        Push an image or a repository to a registry
  rename      Rename a container
  restart     Restart one or more containers
  rm          Remove one or more containers
  rmi         Remove one or more images
  run         Run a command in a new container
  save        Save one or more images to a tar archive (streamed to STDOUT by default)
  search      Search the Docker Hub for images
  start       Start one or more stopped containers
  stats       Display a live stream of container(s) resource usage statistics
  stop        Stop one or more running containers
  tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
  top         Display the running processes of a container
  unpause     Unpause all processes within one or more containers
  update      Update configuration of one or more containers
  version     Show the Docker version information
  wait        Block until one or more containers stop, then print their exit codes

Run 'docker COMMAND --help' for more information on a command.
[root@lb ~]#
[root@lb ~]# docker info
Containers: 1
 Running: 0
 Paused: 0
 Stopped: 1
Images: 1
Server Version: 1.13.1
Storage Driver: overlay2
 Backing Filesystem: xfs
 Supports d_type: true
 Native Overlay Diff: true
Logging Driver: journald
Cgroup Driver: systemd
Plugins:
 Volume: local
 Network: bridge host macvlan null overlay
 Authorization: rhel-push-plugin
Swarm: inactive
Runtimes: docker-runc runc
Default Runtime: docker-runc
Init Binary: /usr/libexec/docker/docker-init-current
containerd version:  (expected: aa8187dbd3b7ad67d8e5e3a15115d3eef43a7ed1)
runc version: e9c345b3f906d5dc5e8100b05ce37073a811c74a (expected: 9df8b306d01f59d3a8029be41                                                                  1de015b7304dd8f)
init version: 5b117de7f824f3d3825737cf09581645abbe35d4 (expected: 949e6facb77383876aeff8a69                                                                  44dde66b3089574)
Security Options:
 seccomp
  WARNING: You're not using the default seccomp profile
  Profile: /etc/docker/seccomp.json
 selinux
Kernel Version: 3.10.0-862.3.2.el7.x86_64
Operating System: Red Hat Enterprise Linux
OSType: linux
Architecture: x86_64
Number of Docker Hooks: 3
CPUs: 4
Total Memory: 7.631 GiB
Name: lb
ID: EMCE:WSOG:3NFM:OYDJ:ZBYG:3DOW:5QA4:2BWD:JVLH:BAYZ:JES6:7G4O
Docker Root Dir: /var/lib/docker
Debug Mode (client): false
Debug Mode (server): false
Username: admin
Registry: https://registry.access.redhat.com/v1/
WARNING: bridge-nf-call-iptables is disabled
WARNING: bridge-nf-call-ip6tables is disabled
Experimental: false
Insecure Registries:
 127.0.0.0/8
Live Restore Enabled: false
Registries: registry.access.redhat.com (secure), docker.io (secure)
[root@lb ~]#
[root@lb ~]# cd /etc/default/docker
-bash: cd: /etc/default/docker: No such file or directory
[root@lb ~]# vi /etc/default/docker
[root@lb ~]# pwd
/root
[root@lb ~]# vi /usr/lib/systemd/docker.service
[root@lb ~]# cd /usr/lib/systemd
[root@lb systemd]# ls
catalog                systemd-cryptsetup         systemd-shutdownd
import-pubring.gpg     systemd-fsck               systemd-sleep
ntp-units.d            systemd-hibernate-resume   systemd-socket-proxyd
rhel-autorelabel       systemd-hostnamed          systemd-sysctl
rhel-configure         systemd-importd            systemd-sysv-install
rhel-dmesg             systemd-initctl            systemd-timedated
rhel-domainname        systemd-journald           systemd-udevd
rhel-import-state      systemd-localed            systemd-update-done
rhel-loadmodules       systemd-logind             systemd-update-utmp
rhel-readonly          systemd-machined           systemd-user-sessions
scripts                systemd-machine-id-commit  systemd-vconsole-setup
system                 systemd-modules-load       system-generators
systemd                systemd-pull               system-preset
systemd-ac-power       systemd-quotacheck         system-shutdown
systemd-activate       systemd-random-seed        system-sleep
systemd-backlight      systemd-readahead          user
systemd-binfmt         systemd-remount-fs         user-generators
systemd-bootchart      systemd-reply-password     user-preset
systemd-cgroups-agent  systemd-rfkill
systemd-coredump       systemd-shutdown
[root@lb systemd]# cd system
[root@lb system]# cd docker.service
-bash: cd: docker.service: Not a directory
[root@lb system]# vi docker.service
[root@lb system]# vi /etc/sysconfig/docker
[root@lb system]# ls
arp-ethers.service                      lvm2-monitor.service                           serial-getty@.service
auditd.service                          lvm2-pvscan@.service                           shutdown.target
auth-rpcgss-module.service              machine.slice                                  shutdown.target.wants
autovt@.service                         machines.target                                sigpwr.target
basic.target                            messagebus.service                             sleep.target
basic.target.wants                      microcode.service                              -.slice
blk-availability.service                multi-user.target                              slices.target
bluetooth.target                        multi-user.target.wants                        smartcard.target
brandbot.path                           NetworkManager-dispatcher.service              sockets.target
brandbot.service                        NetworkManager.service                         sockets.target.wants
chrony-dnssrv@.service                  NetworkManager-wait-online.service             sound.target
chrony-dnssrv@.timer                    network-online.target                          sshd-keygen.service
chronyd.service                         network-pre.target                             sshd.service
chrony-wait.service                     network.target                                 sshd@.service
console-getty.service                   nfs-blkmap.service                             sshd.socket
console-shell.service                   nfs-client.target                              suspend.target
container-getty@.service                nfs-config.service                             swap.target
cpupower.service                        nfs-idmapd.service                             sys-fs-fuse-connections.mount
crond.service                           nfs-idmap.service                              sysinit.target
cryptsetup-pre.target                   nfs-lock.service                               sysinit.target.wants
cryptsetup.target                       nfslock.service                                sys-kernel-config.mount
ctrl-alt-del.target                     nfs-mountd.service                             sys-kernel-debug.mount
dbus-org.freedesktop.hostname1.service  nfs-rquotad.service                            syslog.socket
dbus-org.freedesktop.import1.service    nfs-secure.service                             syslog.target.wants
dbus-org.freedesktop.locale1.service    nfs-server.service                             systemd-ask-password-console.path
dbus-org.freedesktop.login1.service     nfs.service                                    systemd-ask-password-console.service
dbus-org.freedesktop.machine1.service   nfs-utils.service                              systemd-ask-password-plymouth.path
dbus-org.freedesktop.timedate1.service  nss-lookup.target                              systemd-ask-password-plymouth.service
dbus.service                            nss-user-lookup.target                         systemd-ask-password-wall.path
dbus.socket                             paths.target                                   systemd-ask-password-wall.service
dbus.target.wants                       plymouth-halt.service                          systemd-backlight@.service
debug-shell.service                     plymouth-kexec.service                         systemd-binfmt.service
default.target                          plymouth-poweroff.service                      systemd-bootchart.service
default.target.wants                    plymouth-quit.service                          systemd-firstboot.service
dev-hugepages.mount                     plymouth-quit-wait.service                     systemd-fsck-root.service
dev-mqueue.mount                        plymouth-read-write.service                    systemd-fsck@.service
dm-event.service                        plymouth-reboot.service                        systemd-halt.service
dm-event.socket                         plymouth-start.service                         systemd-hibernate-resume@.service
docker-cleanup.service                  plymouth-switch-root.service                   systemd-hibernate.service
docker-cleanup.timer                    polkit.service                                 systemd-hostnamed.service
docker.service                          postfix.service                                systemd-hwdb-update.service
docker-storage-setup.service            poweroff.target                                systemd-hybrid-sleep.service
dracut-cmdline.service                  poweroff.target.wants                          systemd-importd.service
dracut-initqueue.service                printer.target                                 systemd-initctl.service
dracut-mount.service                    proc-fs-nfsd.mount                             systemd-initctl.socket
dracut-pre-mount.service                proc-sys-fs-binfmt_misc.automount              systemd-journal-catalog-update.service
dracut-pre-pivot.service                proc-sys-fs-binfmt_misc.mount                  systemd-journald.service
dracut-pre-trigger.service              psacct.service                                 systemd-journald.socket
dracut-pre-udev.service                 quotaon.service                                systemd-journal-flush.service
dracut-shutdown.service                 rc-local.service                               systemd-kexec.service
ebtables.service                        rdisc.service                                  systemd-localed.service
emergency.service                       reboot.target                                  systemd-logind.service
emergency.target                        reboot.target.wants                            systemd-machined.service
final.target                            registries.service                             systemd-machine-id-commit.service
firewalld.service                       remote-cryptsetup.target                       systemd-modules-load.service
fstrim.service                          remote-fs-pre.target                           systemd-nspawn@.service
fstrim.timer                            remote-fs.target                               systemd-poweroff.service
getty-pre.target                        rescue.service                                 systemd-quotacheck.service
getty@.service                          rescue.target                                  systemd-random-seed.service
getty.target                            rescue.target.wants                            systemd-readahead-collect.service
graphical.target                        rhel-autorelabel-mark.service                  systemd-readahead-done.service
graphical.target.wants                  rhel-autorelabel.service                       systemd-readahead-done.timer
gssproxy.service                        rhel-configure.service                         systemd-readahead-drop.service
halt-local.service                      rhel-dmesg.service                             systemd-readahead-replay.service
halt.target                             rhel-domainname.service                        systemd-reboot.service
halt.target.wants                       rhel-import-state.service                      systemd-remount-fs.service
haproxy.service                         rhel-loadmodules.service                       systemd-rfkill@.service
hibernate.target                        rhel-push-plugin.service                       systemd-shutdownd.service
hybrid-sleep.target                     rhel-push-plugin.socket                        systemd-shutdownd.socket
hypervfcopyd.service                    rhel-readonly.service                          systemd-suspend.service
hypervkvpd.service                      rhsmcertd.service                              systemd-sysctl.service
hypervvssd.service                      rhsm-facts.service                             systemd-timedated.service
initrd-cleanup.service                  rhsm.service                                   systemd-tmpfiles-clean.service
initrd-fs.target                        rpcbind.service                                systemd-tmpfiles-clean.timer
initrd-parse-etc.service                rpcbind.socket                                 systemd-tmpfiles-setup-dev.service
initrd-root-fs.target                   rpcbind.target                                 systemd-tmpfiles-setup.service
initrd-switch-root.service              rpc-gssd.service                               systemd-udevd-control.socket
initrd-switch-root.target               rpcgssd.service                                systemd-udevd-kernel.socket
initrd-switch-root.target.wants         rpcidmapd.service                              systemd-udevd.service
initrd.target                           rpc_pipefs.target                              systemd-udev-settle.service
initrd.target.wants                     rpc-rquotad.service                            systemd-udev-trigger.service
initrd-udevadm-cleanup-db.service       rpc-statd-notify.service                       systemd-update-done.service
ip6tables.service                       rpc-statd.service                              systemd-update-utmp-runlevel.service
iprdump.service                         rsyncd.service                                 systemd-update-utmp.service
iprinit.service                         rsyncd@.service                                systemd-user-sessions.service
iprupdate.service                       rsyncd.socket                                  systemd-vconsole-setup.service
iprutils.target                         rsyslog.service                                system.slice
iptables.service                        runlevel0.target                               system-update.target
irqbalance.service                      runlevel1.target                               teamd@.service
kdump.service                           runlevel1.target.wants                         timers.target
kexec.target                            runlevel2.target                               timers.target.wants
kexec.target.wants                      runlevel2.target.wants                         time-sync.target
kmod-static-nodes.service               runlevel3.target                               tmp.mount
local-fs-pre.target                     runlevel3.target.wants                         tuned.service
local-fs.target                         runlevel4.target                               umount.target
local-fs.target.wants                   runlevel4.target.wants                         user.slice
lvm2-lvmetad.service                    runlevel5.target                               var-lib-nfs-rpc_pipefs.mount
lvm2-lvmetad.socket                     runlevel5.target.wants                         wpa_supplicant.service
lvm2-lvmpolld.service                   runlevel6.target
lvm2-lvmpolld.socket                    selinux-policy-migrate-local-changes@.service
[root@lb system]# cd /etc/docker/daemon.json
-bash: cd: /etc/docker/daemon.json: Not a directory
[root@lb system]# vi /etc/docker/daemon.json
[root@lb system]# docker images
REPOSITORY              TAG                 IMAGE ID            CREATED             SIZE
docker.io/hello-world   latest              fce289e99eb9        3 months ago        1.84 kB
[root@lb system]#
