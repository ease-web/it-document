login as: root
root@192.168.225.122's password:
Last login: Thu May 16 10:11:58 2019 from 192.168.225.253
[root@appnode3 ~]# oc login https://admin.ocp.eabdc:8443 --token=lvjPe6Gk2jCJXPy                                                                             wcJo4IeFy0_dVZ4g06CdXd62ZZnE
Logged into "https://admin.ocp.eabdc:8443" as "admin" using the token provided.

You have access to the following projects and can switch between them with 'oc p                                                                             roject <projectname>':

    121demo
    ax-project-001
    axasgprod
  * axasgsit
    axasgsit2
    axasgsit3
    default
    demo2
    eab
    eab-121-v3-dev
    eab-cafe-dev
    eab-cafe-sit
    eab-ocr-service
    eab-time-sheet
    eab-time-sheet-dev
    eabtimesheet
    ilife5
    iquote-sit
    kube-public
    kube-service-catalog
    kube-system
    logging
    management-infra
    ocr-service-dev
    ocr-service-prod
    ocr-service-sit
    openshift
    openshift-infra
    openshift-metrics
    openshift-node
    openshift-web-console
    php
    sftp
    test
    zurich-demo

Using project "axasgsit".
[root@appnode3 ~]#
[root@appnode3 ~]# oc get node appnode3.ocp.eabdc
NAME                 STATUS    ROLES     AGE       VERSION
appnode3.ocp.eabdc   Ready     compute   359d      v1.9.1+a0ce1bc657
[root@appnode3 ~]# oc describe node appnode3.ocp.eabdc
Name:               appnode3.ocp.eabdc
Roles:              compute
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/hostname=appnode3.ocp.eabdc
                    logging-infra-fluentd=true
                    node-role.kubernetes.io/compute=true
                    region=primary
                    zone=default
Annotations:        volumes.kubernetes.io/controller-managed-attach-detach=true
Taints:             <none>
CreationTimestamp:  Fri, 25 May 2018 12:43:47 -0400
Conditions:
  Type             Status  LastHeartbeatTime                 LastTransitionTime                Reason                                                           Message
  ----             ------  -----------------                 ------------------                ------                                                           -------
  OutOfDisk        False   Mon, 20 May 2019 06:59:59 -0400   Thu, 16 May 2019 10:57:10 -0400   KubeletHasSufficientDisk                                         kubelet has sufficient disk space available
  MemoryPressure   False   Mon, 20 May 2019 06:59:59 -0400   Thu, 16 May 2019 10:57:10 -0400   KubeletHasSufficientMemory                                       kubelet has sufficient memory available
  DiskPressure     False   Mon, 20 May 2019 06:59:59 -0400   Thu, 16 May 2019 10:57:10 -0400   KubeletHasNoDiskPressure                                         kubelet has no disk pressure
  Ready            True    Mon, 20 May 2019 06:59:59 -0400   Thu, 16 May 2019 10:57:20 -0400   KubeletReady                                                     kubelet is posting ready status
Addresses:
  InternalIP:  192.168.225.122
  Hostname:    appnode3.ocp.eabdc
Capacity:
 cpu:     8
 memory:  12130200Ki
 pods:    80
Allocatable:
 cpu:     7800m
 memory:  11503512Ki
 pods:    80
System Info:
 Machine ID:                 a116344dea3f495a85a6a272b3b1db38
 System UUID:                E40DE6E1-985A-0844-90E0-ECC808D6ED4D
 Boot ID:                    2fcc4d9c-86da-4477-867a-94b6065734f1
 Kernel Version:             3.10.0-862.3.2.el7.x86_64
 OS Image:                   Red Hat Enterprise Linux
 Operating System:           linux
 Architecture:               amd64
 Container Runtime Version:  docker://1.13.1
 Kubelet Version:            v1.9.1+a0ce1bc657
 Kube-Proxy Version:         v1.9.1+a0ce1bc657
ExternalID:                  appnode3.ocp.eabdc
Non-terminated Pods:         (14 in total)
  Namespace                  Name                               CPU Requests  CPU Limits  Memory Requests  Memory Limits
  ---------                  ----                               ------------  ----------  ---------------  -------------
  121demo                    ease-api-14-6jhvr                  0 (0%)        0 (0%)      0 (0%)           0 (0%)
  axasgsit                   ease-api-28-wrfnk                  0 (0%)        0 (0%)      0 (0%)           0 (0%)
  axasgsit3                  ease-api-15-lmdj2                  0 (0%)        0 (0%)      0 (0%)           0 (0%)
  axasgsit3                  ease-web-49-p58m9                  0 (0%)        0 (0%)      0 (0%)           0 (0%)
  eab-121-v3-dev             eab-web-29-q782w                   0 (0%)        0 (0%)      0 (0%)           0 (0%)
  eab-cafe-dev               cafe-front-end-76-kwzdd            0 (0%)        0 (0%)      0 (0%)           0 (0%)
  logging                    logging-fluentd-jnljh              100m (1%)     0 (0%)      512Mi (4%)       512Mi (4%)
  openshift-metrics          prometheus-node-exporter-d6g7f     100m (1%)     200m (2%)   30Mi (0%)        50Mi (0%)
  zurich-demo                redis-3-tqwxm                      0 (0%)        0 (0%)      512Mi (4%)       512Mi (4%)
  zurich-demo                signdocweb-20181121-004-3-xdzqc    0 (0%)        0 (0%)      0 (0%)           0 (0%)
  zurich-demo                zurich-admin-6-dldvp               0 (0%)        0 (0%)      0 (0%)           0 (0%)
  zurich-demo                zurich-mobile-api-4-bcft4          0 (0%)        0 (0%)      0 (0%)           0 (0%)
  zurich-demo                zurich-web-11-build                0 (0%)        0 (0%)      0 (0%)           0 (0%)
  zurich-demo                zurich-web-6-sssqz                 0 (0%)        0 (0%)      0 (0%)           0 (0%)
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  CPU Requests  CPU Limits  Memory Requests  Memory Limits
  ------------  ----------  ---------------  -------------
  200m (2%)     200m (2%)   1054Mi (9%)      1074Mi (9%)
Events:         <none>
[root@appnode3 ~]# oc adm drain appnode3.ocp.eabdc --force=true --delete-local-data=true --ignore-daemonsets=true
node "appnode3.ocp.eabdc" cordoned
WARNING: Deleting pods with local storage: redis-3-t9f6z, ease-api-11-build, ease-web-52-build, ease-web-54-build, redis-                                    1-5wp44, mysql-1-p9m8n, eab-web-48-build, eab-web-50-build, eab-web-51-build, cafe-front-end-114-build, cafe-front-end-11                                    9-build, cafe-front-end-120-build, mongodb-1-s5w5f, eap-app-1-build, redis-3-tqwxm, zurich-mobile-api-8-build, zurich-mob                                    ile-api-9-build, zurich-web-10-build, zurich-web-7-build; Ignoring DaemonSet-managed pods: logging-fluentd-jnljh, prometh                                    eus-node-exporter-d6g7f
pod "zurich-web-7-deploy" evicted
pod "ease-api-11-build" evicted
pod "ease-web-52-deploy" evicted
pod "ease-web-52-build" evicted
pod "ease-web-54-build" evicted
pod "eab-web-33-deploy" evicted
pod "eab-web-48-build" evicted
pod "eab-web-50-build" evicted
pod "eab-web-51-build" evicted
pod "cafe-front-end-114-build" evicted
pod "cafe-front-end-119-build" evicted
pod "cafe-front-end-120-build" evicted
pod "cafe-front-end-83-deploy" evicted
pod "eap-app-1-build" evicted
pod "zurich-admin-7-deploy" evicted
pod "zurich-mobile-api-5-deploy" evicted
pod "zurich-mobile-api-8-build" evicted
pod "zurich-mobile-api-9-build" evicted
pod "zurich-web-10-build" evicted
pod "zurich-web-7-build" evicted
^C
[root@appnode3 ~]# oc adm manage-node appnode3.ocp.eabdc --schedulable=false
NAME                 STATUS                     ROLES     AGE       VERSION
appnode3.ocp.eabdc   Ready,SchedulingDisabled   compute   359d      v1.9.1+a0                                                                                ce1bc657
[root@appnode3 ~]# systemctl stop docker atomic-openshift-node
[root@appnode3 ~]# reboot
PolicyKit daemon disconnected from the bus.
We are no longer a registered authentication agent.
