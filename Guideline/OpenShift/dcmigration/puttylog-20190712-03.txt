login as: root
root@192.168.222.214's password:
Last login: Mon Jul  8 16:47:49 2019 from 192.168.223.159
[root@vm-OpenShift-POC-3 ~]# pvs
  PV         VG        Fmt  Attr PSize   PFree
  /dev/sda2  rhel      lvm2 a--  299.00g 141.12g
  /dev/sda3  rhel      lvm2 a--  300.00g      0
  /dev/sdb1  docker-vg lvm2 a--  200.00g   1.54g
[root@vm-OpenShift-POC-3 ~]# oc login https://vm-OpenShift-POC-1:8443 --token=DzXWmpJv_TYq5IIPqPVsUtKC13gKrlIr4X0WKzNH1uA
Logged into "https://vm-OpenShift-POC-1:8443" as "admin" using the token provided.

You have access to the following projects and can switch between them with 'oc project <projectname>':

    121demo
    axasgprod
    axasgsit
    axasgsit2
  * axasgsit3
    cafe-dev
    cafe-prod
    cafe-sit
    default
    eab
    eab-bre-sit
    eab-demo
    eab-document
    eab-es
    eab-timesheet-prod
    hsbc-demo
    hsbcsgsit
    iquote-prod
    kube-system
    logging
    management-infra
    openshift
    openshift-infra
    pias-sg-poc
    project1
    test-automation
    train-alextse
    train-keithchan
    zurich-demo

Using project "axasgsit3".
[root@vm-OpenShift-POC-3 ~]# oc get nodes -o wide
NAME                 STATUS                     AGE       EXTERNAL-IP
vm-openshift-poc-1   Ready,SchedulingDisabled   2y        <none>
vm-openshift-poc-2   Ready                      2y        <none>
vm-openshift-poc-3   Ready                      2y        <none>
[root@vm-OpenShift-POC-3 ~]# oc adm drain vm-openshift-poc-3 --force=true --delete-local-data=true --ignore-daemonsets=true
node "vm-openshift-poc-3" cordoned
WARNING: Deleting pods with local storage: redis-3-r881q, redis-4-k7g3w
pod "ease-api-337-build" evicted
pod "ease-api-335-build" evicted
pod "ease-api-336-build" evicted
pod "ease-api-333-build" evicted
pod "ease-api-79-build" evicted
pod "ease-api-80-build" evicted
pod "ease-web-77-build" evicted
pod "cafe-admin-7-build" evicted
pod "cafe-admin-8-build" evicted
pod "cafe-api-10-build" evicted
pod "cafe-display-12-build" evicted
pod "cafe-admin-8-deploy" evicted
pod "cafe-admin-37-build" evicted
pod "cafe-api-15-deploy" evicted
pod "ease-web-81-build" evicted
pod "cafe-api-6-build" evicted
pod "cafe-api-7-build" evicted
pod "cafe-api-8-build" evicted
pod "cafe-api-3-build" evicted
pod "cafe-api-9-build" evicted
pod "cafe-admin-32-build" evicted
pod "cafe-display-11-build" evicted
pod "cafe-display-10-build" evicted
pod "cafe-display-2-build" evicted
pod "cafe-api-4-build" evicted
pod "cafe-admin-12-build" evicted
pod "cafe-admin-38-build" evicted
pod "cafe-api-15-build" evicted
pod "cafe-display-3-build" evicted
pod "cafe-admin-35-build" evicted
pod "cafe-admin-38-deploy" evicted
pod "cafe-admin-34-build" evicted
pod "cafe-admin-11-build" evicted
pod "cafe-admin-33-build" evicted
pod "cafe-admin-14-build" evicted
pod "cafe-api-14-build" evicted
pod "cafe-api-17-build" evicted
pod "cafe-admin-15-build" evicted
pod "cafe-admin-16-build" evicted
pod "cafe-admin-36-build" evicted
pod "cafe-api-16-build" evicted
pod "cafe-display-8-build" evicted
pod "cafe-admin-18-deploy" evicted
pod "cafe-admin-19-deploy" evicted
pod "cafe-admin-5-build" evicted
pod "ease-web-80-build" evicted
pod "cafe-api-20-build" evicted
pod "cafe-api-27-deploy" evicted
pod "cafe-display-11-build" evicted
pod "cafe-api-28-deploy" evicted
pod "cafe-api-31-deploy" evicted
pod "cafe-display-10-build" evicted
pod "cafe-admin-6-build" evicted
pod "cafe-admin-21-deploy" evicted
pod "cafe-display-13-deploy" evicted
pod "cafe-admin-22-deploy" evicted
pod "ease-api-13-vwjkz" evicted
pod "cafe-display-9-build" evicted
pod "cafe-api-23-build" evicted
pod "cafe-api-22-build" evicted
pod "cafe-api-24-build" evicted
pod "httpd-1-nq2kc" evicted
pod "cafe-api-18-build" evicted
pod "cafe-api-19-build" evicted
pod "cafe-display-9-build" evicted
pod "cafe-display-8-build" evicted
pod "cafe-admin-20-deploy" evicted
pod "cafe-display-12-build" evicted
pod "cafe-api-21-build" evicted
pod "mobile-api-8-xd3sz" evicted
pod "redis-3-r881q" evicted
pod "redis-4-k7g3w" evicted
pod "app-ws-271-c6n28" evicted
pod "signdocweb-poc-1-wb86v" evicted
pod "ease-admin-114-ndl2w" evicted
pod "ease-api-1-6b94r" evicted
pod "ease-spe-agent-10-1pmqd" evicted
pod "ease-admin-12-bv8tp" evicted
pod "ease-web-409-34pbw" evicted
pod "ease-api-263-b6cvj" evicted
pod "mobile-ws-81-3j33f" evicted
pod "ease-admin-3-8b809" evicted
pod "ease-api-87-7g4hz" evicted
pod "sync-gateway-5-35d8c" evicted
pod "cafe-api-11-jz6bp" evicted
pod "app-ws-3-j4hcv" evicted
pod "cafe-display-12-bzrk0" evicted
pod "cafe-api-26-k5fzw" evicted
pod "signdocweb-20181121-004-18-sd0vz" evicted
pod "signdoc-32-88r2v" evicted
pod "cafe-admin-17-cw069" evicted
pod "app-ws-1-5spdj" evicted
pod "cafe-api-14-xk8mr" evicted
pod "cafe-admin-37-8xl01" evicted
pod "cafe-display-16-l30x1" evicted
pod "cafe-admin-7-8lzz3" evicted
node "vm-openshift-poc-3" drained
[root@vm-OpenShift-POC-3 ~]# systemctl stop docker atomic-openshift-node
[root@vm-OpenShift-POC-3 ~]# reboot
