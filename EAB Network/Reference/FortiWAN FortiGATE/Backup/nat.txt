nat {
	wan-array {
		wan@1 {
			1-to-1-rule-array {
				rule { # 1
					log 1
					internal 192.168.222.1
					external 218.189.183.85
				}
			}
			rule-array {
				rule { # 1
					source Group:MailSender
					translated 218.189.183.85
				}
			}
		}
		wan@2 {
			1-to-1-rule-array {
				rule { # 1
					log 1
					internal 192.168.222.1
					external 203.85.238.85
				}
			}
			rule-array {
				rule { # 1
					source Group:MailSender
					translated 203.85.238.85
				}
			}
		}
	}
}
