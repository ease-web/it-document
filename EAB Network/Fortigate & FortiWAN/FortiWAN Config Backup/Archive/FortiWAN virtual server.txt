virtual-server {
	rule-array {
		rule { # 1
			wan-ip 218.189.183.67
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.222.113
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 2
			wan-ip 218.189.183.66
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.222.128
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 3
			wan-ip 203.85.238.92
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.1.77
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 4
			wan-ip 218.189.183.92
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.1.77
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 5
			wan-ip 203.85.238.92
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.1.77
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 6
			wan-ip 218.189.183.92
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.1.77
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 7
			wan-ip 203.85.238.91
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.1.28
					detect ICMP
					service TCP@8443
				}
			}
		}
		rule { # 8
			wan-ip 218.189.183.91
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.1.28
					detect ICMP
					service TCP@8443
				}
			}
		}
		rule { # 9
			wan-ip 203.85.238.91
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.1.28
					detect ICMP
					service TCP@8081
				}
			}
		}
		rule { # 10
			wan-ip 218.189.183.91
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.1.28
					detect ICMP
					service TCP@8081
				}
			}
		}
		rule { # 11
			wan-ip 203.85.238.87
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.222.80
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 12
			wan-ip 218.189.183.87
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.222.80
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 13
			wan-ip 203.85.238.87
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.222.80
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 14
			wan-ip 218.189.183.87
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.222.80
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 15
			wan-ip 203.85.238.86
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.222.32
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 16
			wan-ip 218.189.183.86
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.222.32
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 17
			wan-ip 203.85.238.86
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.222.32
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 18
			wan-ip 218.189.183.86
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.222.32
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 19
			wan-ip 203.85.238.85
			service UDP@995
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service UDP@995
				}
			}
		}
		rule { # 20
			wan-ip 218.189.183.85
			service UDP@995
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service UDP@995
				}
			}
		}
		rule { # 21
			wan-ip 203.85.238.85
			service TCP@995
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service TCP@995
				}
			}
		}
		rule { # 22
			wan-ip 218.189.183.85
			service TCP@995
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service TCP@995
				}
			}
		}
		rule { # 23
			wan-ip 203.85.238.85
			service UDP@993
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service UDP@993
				}
			}
		}
		rule { # 24
			wan-ip 218.189.183.85
			service UDP@993
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service UDP@993
				}
			}
		}
		rule { # 25
			wan-ip 203.85.238.85
			service TCP@993
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service TCP@993
				}
			}
		}
		rule { # 26
			wan-ip 218.189.183.85
			service TCP@993
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service TCP@993
				}
			}
		}
		rule { # 27
			wan-ip 203.85.238.85
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 28
			wan-ip 218.189.183.85
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 29
			wan-ip 203.85.238.85
			service SMTP
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service SMTP
				}
			}
		}
		rule { # 30
			wan-ip 218.189.183.85
			service SMTP
			server-array {
				server { # 1
					server-ip 192.168.222.1
					detect ICMP
					service SMTP
				}
			}
		}
		rule { # 31
			wan-ip 203.85.238.83
			service FTP
			server-array {
				server { # 1
					server-ip 192.168.1.50
					detect ICMP
					service FTP
				}
			}
		}
		rule { # 32
			wan-ip 218.189.183.83
			service FTP
			server-array {
				server { # 1
					server-ip 192.168.1.50
					detect ICMP
					service FTP
				}
			}
		}
		rule { # 33
			wan-ip 203.85.238.83
			service SSH
			server-array {
				server { # 1
					server-ip 192.168.1.50
					detect ICMP
					service SSH
				}
			}
		}
		rule { # 34
			wan-ip 218.189.183.83
			service SSH
			server-array {
				server { # 1
					server-ip 192.168.1.50
					detect ICMP
					service SSH
				}
			}
		}
		rule { # 35
			enable 0
			wan-ip 203.85.238.81
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.1.28
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 36
			enable 0
			wan-ip 203.85.238.81
			service HTTPS
			server-array {
				server { # 1
					server-ip 192.168.1.28
					detect ICMP
					service HTTPS
				}
			}
		}
		rule { # 37
			enable 0
			wan-ip 218.189.183.81
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.1.28
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 38
			enable 0
			wan-ip 203.85.238.81
			service HTTP
			server-array {
				server { # 1
					server-ip 192.168.1.28
					detect ICMP
					service HTTP
				}
			}
		}
		rule { # 39
			wan-ip 218.189.183.88
			server-array {
				server { # 1
					server-ip 192.168.0.6
					detect ICMP
					service Any
				}
			}
		}
		rule { # 40
			wan-ip 203.85.238.88
			server-array {
				server { # 1
					server-ip 192.168.0.6
					detect ICMP
					service Any
				}
			}
		}
	}
}
