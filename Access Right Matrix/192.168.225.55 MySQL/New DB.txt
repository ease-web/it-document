Create DB
mysql> CREATE DATABASE [DB Name];
mysql> USE [DB Name]

Create USER
mysql> CREATE USER '[newuser]'@'[localhost]' IDENTIFIED BY '[user_password]';

Assign USER to DB
mysql> GRANT ALL PRIVILEGES ON [DB Name].* TO '[Username]'@'localhost';